#!/usr/bin/env node
const { program } = require('commander');
const logger = require('./logger');
const Gue = require('./index');

program
  .arguments('<componentName> [directory]')
  .option('-u, --unit', 'create unit test of the component too')
  .option('-ue, --unitExt <extension>', 'define what extension your test file should have (e.g. .spec.js)')
  .option('-rs, --removeSrc', 'whether to strip the \'src\' out of test directory')
  .option('-m, --mirror', 'whether to mirror the directory structure for your unit tests')
  .option('-t, --template <name>', 'define which template to use');

const parameters = program.parse(process.argv);

try {
  // eslint-disable-next-line no-negated-condition
  if (!parameters.args[0]) {
    logger.warn(`
    You must supply a name for your component
    Usage: gue <componentName> [directory] [options]`);
  } else {
    const gue = new Gue(parameters.args[0], parameters.args[1], parameters.opts());
    gue.generate();
  }
} catch (error) {
  logger.error(error);
}
