const JoyCon = require('joycon');

const joycon = new JoyCon();

const conf = joycon.loadSync(['.gue.config.json']);

module.exports = conf;
